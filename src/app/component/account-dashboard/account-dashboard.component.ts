import { DatePipe, formatDate } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AccountInformation } from 'src/app/class/account-information';
import { ApiResponse } from 'src/app/class/api-response';
import { ApiOperationService } from 'src/app/service/api-operation.service';

@Component({
  selector: 'app-account-dashboard',
  templateUrl: './account-dashboard.component.html',
  styleUrls: ['./account-dashboard.component.css']
})
export class AccountDashboardComponent implements OnInit {
  availableAccountInfo!: Map<String, number>;
  todayDate = new Date;
  eventsSubscription!: Subscription;
  @Input() events!: Observable<void>;
  formattedDt = formatDate(new Date(), 'MMM-YYYY', 'en_US');

  constructor(private apiOperation: ApiOperationService,
    public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.eventsSubscription = this.events.subscribe(() => this.refreshData());
    this.getActiveAccountInformation(this.formattedDt);
  }

  refreshData() {
    this.getActiveAccountInformation(this.formattedDt);
  }

  getActiveAccountInformation(formattedDt: String) {
    this.apiOperation.getCurrentMonthTransaction(formattedDt).subscribe((data: ApiResponse) => {
      if(data.responseType=="SUCCESS") {
        this.availableAccountInfo = JSON.parse(JSON.stringify(data.result));
      }else {
        console.log("error");
      }
    });
  }

}
