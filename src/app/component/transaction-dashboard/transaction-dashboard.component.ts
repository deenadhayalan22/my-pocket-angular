import { animate, state, style, transition, trigger } from '@angular/animations';
import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Tile } from '@angular/material/grid-list/tile-coordinator';
import { TooltipPosition } from '@angular/material/tooltip';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ApiResponse } from 'src/app/class/api-response';
import { TransactionHistory, TransactionHistoryRequestDto } from 'src/app/class/transaction-history';
import { ApiOperationService } from 'src/app/service/api-operation.service';
import { SecurityService } from 'src/app/service/security.service';
import { AccountDashboardComponent } from '../account-dashboard/account-dashboard.component';
import { AddTransactionInfoComponent } from '../add-transaction-info/add-transaction-info.component';
import { DeleteTransactionInfoComponent } from '../delete-transaction-info/delete-transaction-info.component';
import { EditTransactionInfoComponent } from '../edit-transaction-info/edit-transaction-info.component';

@Component({
  selector: 'app-transaction-dashboard',
  templateUrl: './transaction-dashboard.component.html',
  styleUrls: ['./transaction-dashboard.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TransactionDashboardComponent implements OnInit {
  displayedColumns: string[] = ['transactionDate', 'amount', 'type', 'actions'];
  recentTransaction: TransactionHistory[] = []
  id?: number;
  expandedElement : boolean = false;
  showawse: boolean =false;
  eventsSubject: Subject<void> = new Subject<void>();
  accountName?: String;

  constructor(private apiOperation: ApiOperationService,
    public dialog: MatDialog,
    private securityService: SecurityService) { }

  ngOnInit(): void {
    this.securityService.getUserName().subscribe(data => this.accountName = data.name);
    this.eventsSubject.next();
    this.getAllTransaction()
  }

  getAllTransaction() {
    const response = this.apiOperation.getAllTransaction();
    response.subscribe((data)=> {
      if(data.responseType === "SUCCESS") {
        this.recentTransaction = JSON.parse(
          JSON.stringify(data.result).replace(/\:null/gi, ':""')
        );
      }
    })
  }
 
  deleteItem(id: number, amount: number, type: string, description: string) {
    this.id = id;
    const dialogRef = this.dialog.open(DeleteTransactionInfoComponent, {
      width: '250px',
      data: {id: id, amount: amount, type: type, description: description}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.getAllTransaction();
        this.eventsSubject.next();
      }
    });
  }

  startEdit(id: number, amount: number, type: string, description: string, transactionDate: Date,
    debitedAccountId: number, creditedAccountId: number) {
    this.id = id;
    const dialogRef = this.dialog.open(EditTransactionInfoComponent, {
      width: '275px',
      data: {id: id, amount: amount, type: type, description: description, transactionDate: transactionDate,
        debitedAccountId: debitedAccountId, creditedAccountId: creditedAccountId}
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.getAllTransaction();
        this.eventsSubject.next();
      }
    });
  }

  addNew() {
    const dialogRef = this.dialog.open(AddTransactionInfoComponent, {
      width: '275px',
      data: {issue: TransactionHistoryRequestDto }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        this.getAllTransaction();
        this.eventsSubject.next();
      }
    });
  }
}
