import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter  } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiOperationService } from 'src/app/service/api-operation.service';

@Component({
  selector: 'app-update-transaction',
  templateUrl: './update-transaction.component.html',
  styleUrls: ['./update-transaction.component.css']
})
export class UpdateTransactionComponent implements OnInit {
  @Input() currentTransaction: any;
  @Output() getResponse = new EventEmitter;

  constructor() { }

    ngOnInit(): void {
      
    }

  onClick() {  
    this.getResponse.emit('Message from child');  
  }

}
