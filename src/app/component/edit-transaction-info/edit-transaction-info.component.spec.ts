import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTransactionInfoComponent } from './edit-transaction-info.component';

describe('EditTransactionInfoComponent', () => {
  let component: EditTransactionInfoComponent;
  let fixture: ComponentFixture<EditTransactionInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTransactionInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTransactionInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
