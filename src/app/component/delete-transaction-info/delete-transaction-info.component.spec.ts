import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTransactionInfoComponent } from './delete-transaction-info.component';

describe('DeleteTransactionInfoComponent', () => {
  let component: DeleteTransactionInfoComponent;
  let fixture: ComponentFixture<DeleteTransactionInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteTransactionInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTransactionInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
