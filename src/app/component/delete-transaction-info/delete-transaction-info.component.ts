import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiOperationService } from 'src/app/service/api-operation.service';

@Component({
  selector: 'app-delete-transaction-info',
  templateUrl: './delete-transaction-info.component.html',
  styleUrls: ['./delete-transaction-info.component.css']
})
export class DeleteTransactionInfoComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteTransactionInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public apiService: ApiOperationService) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.apiService.deleteTransaction(this.data.id).subscribe();
  }

}
