import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountInformation } from 'src/app/class/account-information';
import { ApiOperationService } from 'src/app/service/api-operation.service';

@Component({
  selector: 'app-edit-account-info',
  templateUrl: './edit-account-info.component.html',
  styleUrls: ['./edit-account-info.component.css']
})
export class EditAccountInfoComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditAccountInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AccountInformation,
    public apiOperation: ApiOperationService) { }

  ngOnInit(): void {
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' : '';
  }

  submit() {
    // emppty stuff
    }
  
    onNoClick() {
      this.dialogRef.close();
    }
  
    public stopEdit() {
      this.apiOperation.updateAccountInformation(this.data).subscribe(); 
    }

}
