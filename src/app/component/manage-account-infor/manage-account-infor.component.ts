import { Component, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { AccountInformation } from 'src/app/class/account-information';
import { ApiResponse } from 'src/app/class/api-response';
import { ApiOperationService } from 'src/app/service/api-operation.service';
import { EditAccountInfoComponent } from '../edit-account-info/edit-account-info.component';
import { CollectAccountInfoComponent } from '../shared/collect-account-info/collect-account-info.component';

@Component({
  selector: 'app-manage-account-infor',
  templateUrl: './manage-account-infor.component.html',
  styleUrls: ['./manage-account-infor.component.css']
})
export class ManageAccountInforComponent implements OnInit {

  @ViewChild("viewContainerRef", { read: ViewContainerRef })

  VCR!: ViewContainerRef;
  child_unique_key: number = 0;
  componentsReferences = Array<ComponentRef<CollectAccountInfoComponent>>()

  availableAccountInfo: AccountInformation[] = [];
  isAvailable: boolean = false;

  constructor(private CFR: ComponentFactoryResolver,
    private apiOperation: ApiOperationService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getActiveAccountInformation()
  }

  getActiveAccountInformation() {
    this.apiOperation.getAllAccountInformation().subscribe((data: ApiResponse) => {
      if(data.responseType=="SUCCESS") {
        this.availableAccountInfo = JSON.parse(JSON.stringify(data.result));
        console.log(this.availableAccountInfo);
        this.isAvailable = true;
      }else {
        console.log("error");
      }
    });
  }

  deleteItem(accountInfo: number) {
    this.apiOperation.deleteAccountInformation(accountInfo).subscribe((data: ApiResponse) => {
      console.log(data);
      this.ngOnInit();
    });
  }

  collectAccountInformation() { 
    let componentFactory = this.CFR.resolveComponentFactory(CollectAccountInfoComponent);

    let childComponentRef = this.VCR.createComponent(componentFactory);

    let childComponent = childComponentRef.instance;
    childComponent.unique_key = ++this.child_unique_key;
    childComponent.parentRef = this;

    // add reference for newly created component
    this.componentsReferences.push(childComponentRef);
  }

  remove(key: number) {
    this.ngOnInit()
    if (this.VCR.length < 1) return;

    let componentRef = this.componentsReferences.filter(
      x => x.instance.unique_key == key
    )[0]; 


    let vcrIndex: number = this.VCR.indexOf(componentRef.hostView);

    // removing component from container
    this.VCR.remove(vcrIndex);

    // removing component from the list
    this.componentsReferences = this.componentsReferences.filter(
      x => x.instance.unique_key !== key
    );
  }

  editItem(accountInfo: AccountInformation) {
    const dialogRef = this.dialog.open(EditAccountInfoComponent, {
      width: '275px',
      data: accountInfo
    });
  }

}
