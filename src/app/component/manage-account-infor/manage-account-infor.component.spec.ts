import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageAccountInforComponent } from './manage-account-infor.component';

describe('ManageAccountInforComponent', () => {
  let component: ManageAccountInforComponent;
  let fixture: ComponentFixture<ManageAccountInforComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageAccountInforComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageAccountInforComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
