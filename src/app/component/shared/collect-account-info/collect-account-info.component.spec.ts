import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CollectAccountInfoComponent } from './collect-account-info.component';

describe('CollectAccountInfoComponent', () => {
  let component: CollectAccountInfoComponent;
  let fixture: ComponentFixture<CollectAccountInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CollectAccountInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollectAccountInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
