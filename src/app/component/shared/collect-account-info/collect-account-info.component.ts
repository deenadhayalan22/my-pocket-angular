import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountInformation } from 'src/app/class/account-information';
import { ApiResponse } from 'src/app/class/api-response';
import { ApiOperationService } from 'src/app/service/api-operation.service';
import { ManageAccountInforComponent } from '../../manage-account-infor/manage-account-infor.component';

@Component({
  selector: 'app-collect-account-info',
  templateUrl: './collect-account-info.component.html',
  styleUrls: ['./collect-account-info.component.css']
})
export class CollectAccountInfoComponent implements OnInit {

  public unique_key!: number;
  public parentRef!: ManageAccountInforComponent;
  accountInfo: AccountInformation = new AccountInformation; 

  constructor(
    private apiOperation: ApiOperationService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' : '';
  }

  submit() {
    this.apiOperation.addAccountInformation(this.accountInfo).subscribe((data: ApiResponse) => {
      debugger
      if(data.responseType == "SUCCESS") {
        this.parentRef.remove(this.unique_key)
      }else {
        console.log("error")
      }
    })
    }

    remove_me() {
      console.log(this.unique_key)
      this.parentRef.remove(this.unique_key)
    }
 
}
