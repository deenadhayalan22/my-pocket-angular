import { Component, OnInit } from '@angular/core';
import { SecurityService } from 'src/app/service/security.service';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {

  

  constructor(private securityService: SecurityService) { }

  ngOnInit(): void {
  }

  removeToken() {
    this.securityService.removeToken();
  }

}
