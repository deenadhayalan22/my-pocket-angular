import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTransactionInfoComponent } from './add-transaction-info.component';

describe('AddTransactionInfoComponent', () => {
  let component: AddTransactionInfoComponent;
  let fixture: ComponentFixture<AddTransactionInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTransactionInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTransactionInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
