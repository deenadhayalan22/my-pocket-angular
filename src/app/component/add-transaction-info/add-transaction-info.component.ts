import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AccountInformation } from 'src/app/class/account-information';
import { ApiResponse } from 'src/app/class/api-response';
import { TransactionHistory, TransactionHistoryRequestDto } from 'src/app/class/transaction-history';
import { ApiOperationService } from 'src/app/service/api-operation.service';

@Component({
  selector: 'app-add-transaction-info',
  templateUrl: './add-transaction-info.component.html',
  styleUrls: ['./add-transaction-info.component.css']
})
export class AddTransactionInfoComponent implements OnInit {

  availableAccountInfo: AccountInformation[] = [];

  constructor(public dialogRef: MatDialogRef<AddTransactionInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TransactionHistoryRequestDto,
    public apiOperation: ApiOperationService,
    private router: Router,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.data.transactionDate = new Date()
    this.getActiveAccountInformation();
  }

  getActiveAccountInformation() {
    this.apiOperation.getAllAccountInformation().subscribe((data: ApiResponse) => {
      if(data.responseType=="SUCCESS") {
        this.availableAccountInfo = JSON.parse(JSON.stringify(data.result));
      }else {
        console.log("error");
      }
    });
  }

  formControl = new FormControl('', [
    Validators.required
  ]);

  

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' : '';
  }
  
    onNoClick() {
      this.dialogRef.close();
    }
  
    public confirmAdd() {
      if(this.data.creditedAccountId ==null && this.data.debitedAccountId == null) {
        let snackBarRef = this._snackBar.open("Either Debit/Credit account should be selected. To add some accounts please click action item", "Navigate");
        snackBarRef.onAction().subscribe(() => {
          this.router.navigateByUrl("/account/info");
        });
      }else {
        this.apiOperation.addTransaction(this.data).subscribe();
        this._snackBar.open("New transaction added");
      }
    }

}
