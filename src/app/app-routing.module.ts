import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallbackComponent } from './component/callback/callback.component';
import { LoginComponent } from './component/login/login.component';
import { ManageAccountInforComponent } from './component/manage-account-infor/manage-account-infor.component';
import { TransactionDashboardComponent } from './component/transaction-dashboard/transaction-dashboard.component';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'home', component:  TransactionDashboardComponent, canActivate: [AuthGuard]},
  {path: 'account/info', component: ManageAccountInforComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'callback', component: CallbackComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
