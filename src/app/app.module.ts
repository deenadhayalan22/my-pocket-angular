import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSelectModule} from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDividerModule} from '@angular/material/divider';
import { MAT_DATE_LOCALE } from '@angular/material/core'
import { LoginComponent } from './component/login/login.component';
import { CallbackComponent } from './component/callback/callback.component';
import { AuthHeaderInterceptor } from './interceptor/auth-header.interceptor';
import { UpdateTransactionComponent } from './component/update-transaction/update-transaction.component';
import { AddTransactionInfoComponent } from './component/add-transaction-info/add-transaction-info.component';
import { EditTransactionInfoComponent } from './component/edit-transaction-info/edit-transaction-info.component';
import { DeleteTransactionInfoComponent } from './component/delete-transaction-info/delete-transaction-info.component';
import { TransactionDashboardComponent } from './component/transaction-dashboard/transaction-dashboard.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ManageAccountInforComponent } from './component/manage-account-infor/manage-account-infor.component';
import { CollectAccountInfoComponent } from './component/shared/collect-account-info/collect-account-info.component';
import { EditAccountInfoComponent } from './component/edit-account-info/edit-account-info.component';
import { AccountDashboardComponent } from './component/account-dashboard/account-dashboard.component';
import { DatePipe } from '@angular/common';
import { AppHeaderComponent } from './component/shared/app-header/app-header.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CallbackComponent,
    UpdateTransactionComponent,
    AddTransactionInfoComponent,
    EditTransactionInfoComponent,
    DeleteTransactionInfoComponent,
    TransactionDashboardComponent,
    ManageAccountInforComponent,
    CollectAccountInfoComponent,
    EditAccountInfoComponent,
    AccountDashboardComponent,
    AppHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule, MatInputModule, MatPaginatorModule, MatSortModule, MatToolbarModule, 
    MatTableModule, MatButtonModule, MatIconModule, MatDatepickerModule, MatNativeDateModule,
    MatSelectModule, MatMenuModule, MatTooltipModule, MatSidenavModule, MatCardModule, MatGridListModule,
    MatIconModule, MatDividerModule, MatSnackBarModule
  ],
  providers: [
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHeaderInterceptor,
      multi: true
    },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
