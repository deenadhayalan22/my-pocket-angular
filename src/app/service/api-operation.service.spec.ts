import { TestBed } from '@angular/core/testing';

import { ApiOperationService } from './api-operation.service';

describe('ApiOperationService', () => {
  let service: ApiOperationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiOperationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
