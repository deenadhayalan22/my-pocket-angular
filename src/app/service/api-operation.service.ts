import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ApiResponse } from '../class/api-response';
import { TransactionHistory } from '../class/transaction-history';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AccountInformation } from '../class/account-information';

@Injectable({
  providedIn: 'root'
})
export class ApiOperationService {

  private baseUrl = environment.baseUrl;

  constructor(private http : HttpClient) { }

  getAllTransaction() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + "transaction/record");
  }

  getTransactionById(transactionId: number) : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + "transaction/" + transactionId +"/record");
  }

  addTransaction(transaction: TransactionHistory) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "transaction/record", JSON.stringify(transaction));
  }

  updateTransaction(transaction: TransactionHistory) : Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + "transaction/record", JSON.stringify(transaction));
  }

  deleteTransaction(transactionID: number) : Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "transaction/" +transactionID + "/record");
  }

  addAccountInformation(accountInfo: AccountInformation) : Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + "account/detail", JSON.stringify(accountInfo));
  }

  getAllAccountInformation() : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + "account/detail");
  }

  deleteAccountInformation(accountInfoId: number) : Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + "account/" +accountInfoId + "/detail");
  }

  updateAccountInformation(accountInfo: AccountInformation) : Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + "account/detail", JSON.stringify(accountInfo));
  }

  getCurrentMonthTransaction(period: String) : Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + "budget/" + period +"/portfolio");
  }
}
