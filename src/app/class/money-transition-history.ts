export class MoneyTransitionHistory {
    debitedAccountId?:  number;	
	debitedAccountAlias?: String;	
	debitedValue?: number;
	creditedAccountId?: number;	
	creditedAccountAlias?: String;	
	creditedValue?: number;
}
