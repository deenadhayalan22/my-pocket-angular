import { MoneyTransitionHistory } from "./money-transition-history";

export class TransactionHistory {
    id?: number;
    type?: String;
    description?: String;
    amount?: number;
    transactionDate?: Date;
    moneyTransitionHistory?: MoneyTransitionHistory;
}

export class TransactionHistoryRequestDto {
    id?: number;
    type?: String;
    description?: String;
    amount?: number;
    transactionDate?: Date;
    debitedAccountId?: number;
    creditedAccountId?: number;
}
