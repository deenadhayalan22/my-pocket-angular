export class AccountInformation {
    id!: number;
    accountAlias?: String;
    accountDescription?: String;
    accountValue?: number;
}
